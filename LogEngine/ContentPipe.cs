﻿using System;
using System.Drawing.Imaging;
using System.Drawing;
using OpenTK.Graphics.OpenGL;

namespace LogEngine
{
    public class ContentPipe
    {

        /// <summary>
        /// Loads a texture and returns the OpenGL texture id.
        /// </summary>
        /// <returns>Texture2D</returns>
        /// <param name="filePath">File path (Ommit "Content/").</param>
        public static Texture2D LoadTexture(string filePath)
        {

            filePath = "Content/" + filePath;

            Bitmap bmp = new Bitmap(filePath);
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            int id = GL.GenTexture();

            GL.BindTexture(TextureTarget.Texture2D, id);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmp.Width, bmp.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bmpData.Scan0);

            bmp.UnlockBits(bmpData);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            int width = bmp.Width;
            int height = bmp.Height;

            return new Texture2D(id, width, height);

        }

    }
}
