﻿using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Collections.Generic;
using OpenTK;

namespace LogEngine
{
    public struct Background
    {

        private BackgroundType type;
        private object data;

        public BackgroundType Type { get { return type; } set { type = value; }}
        public object Data { get { return data; } set { data = value; }}

        public Background(BackgroundType type, object data)
        {

            this.type = type;
            this.data = data;

        }

    }

    public class Game
    {

        private GameWindow window;
        private Background background;
        private List<Sprite> sprites = new List<Sprite>();

        public Background Background { get { return background; } set { background = value; }}
        public GameWindow Window { get { return window; }}

        public delegate void Tick();
        public delegate void Load();
        public delegate void Closing();

        public event Tick tick;
        public event Load load;
        public event Closing closing;

        public Game(ref GameWindow window)
        {

            this.window = window;

			this.window.Load += Window_Load;
			this.window.UpdateFrame += Window_UpdateFrame;
			this.window.RenderFrame += Window_RenderFrame;
			this.window.Closing += Window_Closing;

        }

        public Game(int windowWidth, int windowHeight)
        {

            this.window = new GameWindow(windowWidth, windowHeight);

            this.window.Load += Window_Load;
            this.window.UpdateFrame += Window_UpdateFrame;
            this.window.RenderFrame += Window_RenderFrame;
            this.window.Closing += Window_Closing;

        }

        public void Run()
        {

            window.Run();

        }

        public void Close()
        {

            closing();
            window.Close();

        }

        public void addSprite(Sprite sprite)
        {

            if (!sprites.Contains(sprite))
                sprites.Add(sprite);

        }

        private void Window_Load(object sender, System.EventArgs e)
        {

            load();

            SpriteBatch.Initialize();
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void Window_UpdateFrame(object sender, FrameEventArgs e)
        {

            List<Vertex> verts = new List<Vertex>();

            tick();
            
            //Add all the sprites to the vertex buffer
            foreach (Sprite sprite in sprites)
            {

                Vector2[] tVerts =
                {
                    new Vector2(0, 0),
                    new Vector2(0, 1),
                    new Vector2(1, 1),
                    new Vector2(1, 0)
                };

                foreach (Vector2 v in tVerts)
                {

                    Vector2 position = new Vector2(sprite.SpriteRect.X + (v.X * sprite.SpriteRect.Width), sprite.SpriteRect.Y + (v.Y * sprite.SpriteRect.Height));
                    verts.Add(new Vertex(position, v, sprite.Color));

                }

            }

            SpriteBatch.VertexCount = 0;
            SpriteBatch.AddVertexData(verts.ToArray());

        }

        private void Window_RenderFrame(object sender, FrameEventArgs e)
        {

            //Add the modelview matrix
            Matrix4 projMat = Matrix4.CreateOrthographicOffCenter(0, window.Width, window.Height, 0, 0, 1);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref projMat);

            //Enable the Vertex Array
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.ColorArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);

            //Setup the VBO for drawing.
            GL.VertexPointer(2, VertexPointerType.Float, Vertex.SizeInBytes, 0);   
            GL.TexCoordPointer(2, TexCoordPointerType.Float, Vertex.SizeInBytes, Vector2.SizeInBytes);
            GL.ColorPointer(4, ColorPointerType.Float, Vertex.SizeInBytes, Vector2.SizeInBytes * 2);

            if (background.Type == BackgroundType.Color && !(background.Data == null))
            {

                GL.ClearColor((Color)background.Data);

            }

            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.BindTexture(TextureTarget.Texture2D, 0);

            //Drawing code.
            SpriteBatch.DrawBuffer(SpriteBatch.VertexBuffer);

            window.SwapBuffers();

        }
    }
}
