﻿using OpenTK;

namespace LogEngine
{
    public class Player
    {

        private Vector2 position;
        private int width;
        private int height;

        public Vector2 Position { get { return position; } set { position = value; } }
        public int Width { get { return width; } set { width = value; } }
        public int Height { get { return height; } set { height = value; } }

        /// <summary>
        /// Initializes a new instance of the Player class.
        /// </summary>
        /// <param name="width">Player width in pixels.</param>
        /// <param name="height">Player height in pixels.</param>
        /// <param name="position">Player position. (Rounded down to nearest pixel)</param>
        public Player(int width, int height, Vector2 position)
        {

            this.position = position;
            this.width = width;
            this.height = height;

        }

    }
}
