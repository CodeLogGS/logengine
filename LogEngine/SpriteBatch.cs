﻿using System;
using OpenTK;
using System.Drawing;
using OpenTK.Graphics.OpenGL;

namespace LogEngine
{
    
    public struct VBO
    {

        int id;

        public int ID { get { return id; } set { id = value; }}

        public VBO(int id)
        {

            this.id = id;

        }

    }

    public struct Vertex
    {

        private Vector2 position;
        private Vector2 texCoord;
        private Vector4 color;

        public static int SizeInBytes = Vector2.SizeInBytes * 2 + Vector4.SizeInBytes;

        public Vector2 Position { get { return position; } set { position = value; }}
        public Vector2 TexCoord { get { return texCoord; } set { texCoord = value; }}

        public Color Color 
        {

            get
            {

                return Color.FromArgb((int)(color.W * 255), (int)(color.X * 255), (int)(color.Y * 255), (int)(color.Z * 255));

            }

            set
            {

                color = new Vector4(value.R / 255f, value.G / 255f, value.B / 255f, value.A / 255f);

            }

        }

        public Vertex(Vector2 position, Vector2 texCoord,Vector4 color)
        {

            this.position = position;
            this.texCoord = texCoord;
            this.color = color;

        }

		public Vertex(Vector2 position, Vector2 texCoord, Color color)
		{

			this.position = position;
			this.texCoord = texCoord;
            this.color = new Vector4(color.R / 255f, color.G / 255f, color.B / 255f, color.A / 255f);

		}

    }

    public enum BackgroundType
    {
        Color,
        Texture
    }

    public class SpriteBatch
    {

        public static VBO VertexBuffer;
        public static int VertexCount = 0;

        public static void Initialize()
        {

            //Generate a buffer allocation
            VertexBuffer = new VBO(GL.GenBuffer());
            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBuffer.ID);

            //Unbind the buffer for technical reasons :)
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

        }

        public static void AddVertexData(Vertex[] vertices)
        {

            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBuffer.ID);
            GL.BufferData<Vertex>(BufferTarget.ArrayBuffer, (IntPtr)(Vertex.SizeInBytes * vertices.Length), vertices, BufferUsageHint.StaticDraw);
            VertexCount += vertices.Length;

        }

        public static void DrawBuffer(VBO vbo)
        {

            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo.ID);
            GL.DrawArrays(PrimitiveType.Quads, 0, VertexCount);
            
        }

    }
}
