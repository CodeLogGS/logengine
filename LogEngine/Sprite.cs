﻿using OpenTK;
using System.Drawing;

namespace LogEngine
{
    public class Sprite
    {

        private Rectangle spriteRect;
        private Texture2D texture;
        private Color color;

        public Rectangle SpriteRect { get { return spriteRect; } set { spriteRect = value; } }
        public Texture2D Texture { get { return texture; } set { texture = value; } }
        public Color Color { get { return color; } set { color = value; } }

        public Sprite(Texture2D texture, Rectangle spriteRect)
        {

            this.spriteRect = spriteRect;
            this.texture = texture;
            color = Color.White;

        }

        public Sprite(Color color, Rectangle spriteRect)
        {

            this.spriteRect = spriteRect;
            texture = new Texture2D(0, 0, 0);
            this.color = color;

        }

    }
}
